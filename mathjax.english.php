<?php

$txt['mathjax-inline'] = 'Inserts a mathjax line on paragraph';
$txt['mathjax-block'] = 'Inserts a centered mathjax block';
$txt['mathjax-credits'] = '<a href="https://gitlab.com/moyack/mathjax-for-smf2.1.x" title="Mathjax BBCode. A mod SMF 2.1.x" target="_blank"><strong>MathJax for SMF 2.1.X</strong></a> A modification from the work of <a href="https://custom.simplemachines.org/index.php?mod=4077">Digger</a> by Moyack.';
?>
