<?php
/**
 * @package SMF MathJax Mod
 * @author digger, moyack
 * @copyright 2022
 * @license GPLv3
 * @file Subs-MathJax.php
 * @version 1.4.4
 */

if (!defined('SMF'))
	die('No direct access...');

/**
 * Load all needed hooks
 */
function loadMathJaxHooks()
{
    add_integration_function('integrate_load_theme', 'loadMathJaxJs', false, __FILE__);
	add_integration_function('integrate_bbc_codes', 'addMathJaxBbcCode', false, __FILE__);
	add_integration_function('integrate_bbc_buttons', 'addMathJaxBbcButton', false, __FILE__);
	add_integration_function('integrate_credits', 'addMathjaxCopyright', false, __FILE__);
}


/**
 * Load JS libraries
 */
function loadMathJaxJs()
{
	global $modSettings, $context;

	$context['html_headers'] .= !(SMF === 'BACKGROUND' || SMF === 'SSI' || empty($modSettings['enableBBC'])) ? "\n\t" . '<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>' . "\n\t" . '<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>' : '<!-- No mathjax inserted -->';
	if ($context['current_action'] === 'post')
	    $context['insert_after_template'] .= '
	    <script>
	        let target = document.querySelector("#preview_body");
            let observer = new MutationObserver(function(mathjax_mutations) {
                MathJax.typeset([target]);
            });
            observer.observe(target, {characterData: true, childList: true});
	    </script>';
}


/**
 * Add editor buttons
 * @param $bbc_tags
 */

function addMathJaxBbcButton(&$bbc_tags)
{
	global $txt;

	// Load language
	loadLanguage('mathjax');

	// Add Mathjax bbc buttons new group after 'justify' button.
	$indent_bbc = array();
	foreach ($bbc_tags[0] as $tag)
	{
		$indent_bbc[] = $tag;
		if (isset($tag['code']) && $tag['code'] == 'justify')
		{
			$indent_bbc[] = array(
			);
			$indent_bbc[] = array(
				'image' => 'latex',
				'code' => 'latex',
				'before' => '[latex]',
				'after' => '[/latex]',
				'description' => $txt['mathjax-block'],
			);
			$indent_bbc[] = array(
				'image' => 'latex_inline',
				'code' => 'latex_inline',
				'before' => '[latex=inline]',
				'after' => '[/latex]',
				'description' => $txt['mathjax-inline'],
			);
		}
	}
	$bbc_tags[0] = $indent_bbc;
}

/**
 * Add latex bbc
 * @param $codes
 */
function addMathJaxBbcCode(&$codes)
{
    $codes[] = array(
        'tag' => 'latex',
        'type' => 'unparsed_content',
        'content' => '\[ $1 \]',
    );

    $codes[] = array(
        'tag' => 'latex',
        'type' => 'unparsed_equals_content',
        'content' => '\( $1 \)',
    );
}


/**
 * Adds mod copyright to the forum credit's page
 */
function addMathjaxCopyright()
{
    global $context, $txt;

    // Load language
	loadLanguage('mathjax');

	$context['credits_modifications'][] = '
	<a href="https://www.mathjax.org">
	    <img title="Powered by MathJax" src="https://www.mathjax.org/badge/mj_logo.png" alt="Powered by MathJax" style="height: 2em; float: left; padding-right: 1em; margin-top: 0.5em;">
    </a>' . $txt['mathjax-credits'];
}
