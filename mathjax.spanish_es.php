<?php

$txt['mathjax-inline'] = 'Inserta una línea de mathjax dentro del parrafo';
$txt['mathjax-block'] = 'Inserta un bloque centrado de mathjax';
$txt['mathjax-credits'] = '<a href="https://gitlab.com/moyack/mathjax-for-smf2.1.x" title="Mathjax BBCode. Una modificación para SMF 2.1.x" target="_blank"><strong>MathJax para SMF 2.1.X</strong></a>. Una modificación basada en el trabajo de <a href="https://custom.simplemachines.org/index.php?mod=4077">Digger</a>, por Moyack.';
?>
