# Mathjax for SMF2.1.X

<img src="https://static.simplemachinesweb.com/smf/smsite/images/custom/frontpage_logo_large.png" alt="Simple Machines"  width="400"/> <img src="https://www.mathjax.org/badge/badge-square.svg" alt="Mathjax">


## Description
A mod for [Simple Machines Forum, version 2.1.X.](https://www.simplemachines.org/). This mod in particular adds functionality to show MathJax formulas in your webpage. MathJax is the modern JavaScript-based LaTeX rendering solution for the Internet. This mod uses the MathJax CDN instead a local file. The CDN will automatically arrange for your readers to download MathJax files from a fast, nearby server. And since bug fixes and patches are deployed to the CDN as soon as they become available, your pages will always be up to date with the latest browser and devices.

![Example of Mathjax on SMF](https://gitlab.com/moyack/mathjax-for-smf2.1.x/-/raw/main/Mathjax_SMF2.1.jpg)

## Installation

1. First download the latest version of this app in the [Release](https://gitlab.com/moyack/mathjax-for-smf2.1.x/-/releases) section or in the [mod page at SMF webpage](https://custom.simplemachines.org/index.php?mod=4347).
2. Just enter to the _Admin section_ and then to the _Package manager_, then in the option of _Add Packages_ appears an upload file control. Just select the compressed file previously downloaded and folow the steps in your forum setup.
